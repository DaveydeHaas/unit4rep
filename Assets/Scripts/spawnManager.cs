﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnManager : MonoBehaviour
{
    public GameObject enemyPreFab;
    public GameObject PowerUpPreFab;
    private float spawnRange = 9.0f;
    public int EnemyCount;
    public int WaveNumber = 1;
    
    // Start is called before the first frame update
    void Start()
    {
        SpawnEnemyWave(WaveNumber);
        Instantiate(PowerUpPreFab, GenerateSpawnPosition(), PowerUpPreFab.transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        EnemyCount = FindObjectsOfType<Enemy>().Length;

        if (EnemyCount == 0)
        {
            WaveNumber++;
            SpawnEnemyWave(WaveNumber);
            Instantiate(PowerUpPreFab, GenerateSpawnPosition(), PowerUpPreFab.transform.rotation);
        }
    }

    void SpawnEnemyWave(int EnemiesToSpawn)
    {
        for (int i = 0; i < EnemiesToSpawn; i++)
        {
            Instantiate(enemyPreFab, GenerateSpawnPosition(), enemyPreFab.transform.rotation);
        } 
    }
    private Vector3 GenerateSpawnPosition()
    {
        float spawnPosX = Random.Range(-spawnRange, spawnRange);
        float spawnPosZ = Random.Range(-spawnRange, spawnRange);
        
        Vector3 randomPos = new Vector3(spawnPosX, 0, spawnPosZ);

        return randomPos;
    }
}
